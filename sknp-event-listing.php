<?php
/*
Plugin Name: Easy Event Listing
Plugin URI: https://bitbucket.org/sakaruk/sknp-event-listing/
Description: A WordPress plugin making the event listing easier
Version: 1.0
Author: Sakar U Khatiwada
Author URI: https://sakaru.com.np
License: GPL2
*/
/*The boilerplate of the plugin is based on the post https://www.yaconiello.com/blog/how-to-write-wordpress-plugin*/
if(!class_exists('Sknp_Event_Listing'))
{
    class Sknp_Event_Listing
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // register actions
        } // END public function __construct
    
        /**
         * Activate the plugin
         */
        public static function activate()
        {
            // Do nothing
        } // END public static function activate
    
        /**
         * Deactivate the plugin
         */     
        public static function deactivate()
        {
            // Do nothing
        } // END public static function deactivate
    } // END class Sknp_Event_Listing
} // END if(!class_exists('Sknp_Event_Listing'))
if(class_exists('Sknp_Event_Listing'))
{
    // Installation and uninstallation hooks
    register_activation_hook(__FILE__, array('Sknp_Event_Listing', 'activate'));
    register_deactivation_hook(__FILE__, array('Sknp_Event_Listing', 'deactivate'));

    // instantiate the plugin class
    $event_listing = new Sknp_Event_Listing();
}
